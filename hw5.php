<?php

require_once('common.php');

const BASE_URL = 'http://localhost:8080';

class Hw5Tests extends HwTests {

    function baseUrlResponds() {
        $this->get(BASE_URL);
        $this->assertResponse(200);
    }

    function listPageHasMenuWithCorrectLinkIds() {
        $this->get(BASE_URL);
        $this->assertLinkById('list-page-link');
        $this->assertLinkById('add-page-link');
    }

    function applicationLinksShouldBeInCorrectFormat() {
        $this->get(BASE_URL);
        $this->assertFrontControllerLink('list-page-link');
        $this->assertFrontControllerLink('add-page-link');
    }

    function addPageHasCorrectElements() {
        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $this->assertField('firstName');
        $this->assertField('lastName');
        $this->assertField('phone1');
        $this->assertField('phone2');
        $this->assertField('phone3');

        $this->assertField('submitButton');
    }

    function submittingFormAddsPersonToList() {

        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $person = getSampleData();

        $this->setFieldByName('firstName', $person->firstName);
        $this->setFieldByName('lastName', $person->lastName);
        $this->setFieldByName('phone1', $person->phone1);
        $this->setFieldByName('phone2', $person->phone2);
        $this->setFieldByName('phone3', $person->phone3);

        $this->clickSubmitByName('submitButton');

        $this->assertText($person->firstName);
        $this->assertText($person->lastName);
        $this->assertText($person->phone1);
        $this->assertText($person->phone2);
        $this->assertText($person->phone3);
    }

    function makesRedirectAfterFormSubmission() {
        $this->get(BASE_URL);

        $this->clickLinkById('add-page-link');

        $this->setMaximumRedirects(0);

        $this->clickSubmitByName('submitButton');

        $this->assertResponse(302);

        $source = $this->getBrowser()->getContentAsText();

        $this->assertNoPattern('/[<>\w\d]/',
            "Should not print any output along with Location header " .
            "but output was: \n $source");
    }
}

(new Hw5Tests())->run(new PointsReporter());
